﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Microsoft.VisualBasic;

namespace HomeImportC
{
    internal class Program
    {

        private string sCON = "server=104.168.205.130;database=veritashome;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E";
        private string SQL, sLine;
        private string[] sIn; 
        private clsDBO clC = new clsDBO();
        private clsDBO clS = new clsDBO();
        private long lProgramID;

        public void Main(string[] args)
        {
            CopyVSC360();
            CopyForte();
            mMoxy.ImportMoxy();
            RateNewContracts();
            CalcCommission();
        }

        private void CalcCommission()
        {
            clsDBO clCH  = new clsDBO();
            clsDBO clR = new clsDBO();
            double dCancel;
            var clComm = new clsCommission();
            SQL = "select * from contract where credate >= '" + DateTime.Today + "' ";
            //SQL = SQL + "where dealerid = 3 ";
            //SQL = SQL + "where contractno = 'HAS10710008'";
            clC.OpenDB(SQL, sCON);
            if (clC.RowCount() > 0)
            {
                for (int i = 0; i < clC.RowCount(); i++)
                {
                    clC.GetRowNo(i);
                    clComm.ContractID = Convert.ToInt64(clC.GetFields("contractid"));
                    clComm.CalcComm();
                }
            }
        }

        private double GetCancelAmt(long xContractID, double xAmt)
        {
            clsDBO clCC = new clsDBO();
            SQL = "select * from contractcancel cc inner join contract c on c.contractid = cc.contractid " +
                "where status = 'cancelled' and c.contractid = " + xContractID;
            clCC.OpenDB(SQL, sCON);
            if (clCC.RowCount() > 0)
            {
                clCC.GetRow();
                return xAmt * Convert.ToDouble(clCC.GetFields("cancelfactor"));
            }

            return 0;
        }

        private void ClearCommission()
        {
            clsDBO clR = new clsDBO();
            SQL = "delete contractamt where contractid in (select contractid from contract where contractid = " + clC.GetFields("contracid") + ")" +
                " and ratetypeid > 5 ";
            clR.RunSQL(SQL, sCON);
        }

        private void FixSurcharge()
        {
            clsDBO clR = new clsDBO();
            SQL = "select * from contract c inner join contractsurcharge cs on cs.contractid = c.contractid where cs.surchargeid = 13 ";
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                for (int i = 0; i < clR.RowCount(); i++)
                {
                    clR.GetRowNo(i);
                    if (clR.GetFields("plantypeid") == "3")
                    {
                        DateTime d1 = Convert.ToDateTime(clR.GetFields("saledate"));
                        DateTime d2 = Convert.ToDateTime("9/13/2021");
                        if (DateTime.Compare(d1, d2) < 0 )
                        {
                            SQL = "update contract ";
                            SQL = SQL + "set plantypeid = 25 ";
                            SQL = SQL + "where contractid = " + clR.GetFields("contractid");
                            clR.RunSQL(SQL, sCON);
                            SQL = "delete contractsurcharge ";
                            SQL = SQL + "where contractsurchargeid = " + clR.GetFields("contractsurchargeid");
                            clR.RunSQL(SQL, sCON);
                        }
                    }
                }
            }

            SQL = "select * from contract c inner join ContractSurcharge cs on cs.ContractID = c.ContractID " +
                    "where cs.SurchargeID = 14 ";
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                for (int i = 0; i < clR.RowCount();i++)
                {
                    clR.GetRowNo(i);
                    if (clR.GetFields("plantypeid") == "3")
                    {
                        DateTime d1 = Convert.ToDateTime(clR.GetFields("saledate"));
                        DateTime d2 = Convert.ToDateTime("9/13/2021");
                        if (DateTime.Compare(d1, d2) < 0)
                        {
                            SQL = "update contract ";
                            SQL = SQL + "set plantypeid = 26 ";
                            SQL = SQL + "where contractid = " + clR.GetFields("contractid");
                            clR.RunSQL(SQL, sCON);
                            SQL = "delete contractsurcharge ";
                            SQL = SQL + "where contractsurchargeid = " + clR.GetFields("contractsurchargeid");
                            clR.RunSQL(SQL, sCON);
                        }
                    }

                    if (clR.GetFields("plantypeid") == "2")
                    {
                        DateTime d1 = Convert.ToDateTime(clR.GetFields("saledate"));
                        DateTime d2 = Convert.ToDateTime("9/13/2021");
                        if (DateTime.Compare(d1, d2) < 0)
                        {
                            SQL = "update contract ";
                            SQL = SQL + "set plantypeid = 28 ";
                            SQL = SQL + "where contractid = " + clR.GetFields("contractid");
                            clR.RunSQL(SQL, sCON);
                            SQL = "delete contractsurcharge ";
                            SQL = SQL + "where contractsurchargeid = " + clR.GetFields("contractsurchargeid");
                            clR.RunSQL(SQL, sCON);
                        }
                    }
                }
            }
        }

        private void CopyForte()
        {
            var di = new DirectoryInfo(@"C:\inetpub\ftproot\forte\");
            FileInfo[] fiary = di.GetFiles("*.txt");
            string sFile;

            foreach (FileInfo fi in fiary)
            {
                sFile = fi.Name;
                File.Copy(@"C:\inetpub\ftproot\forte\" + fi.Name, @"C:\Home\" + fi.Name);
                File.Delete(@"C:\inetpub\ftproot\forte\" + fi.Name);
                if (!sFile.ToLower().Contains("surcharge"))
                    ImportHomeContractWithHeader(@"c:\home\" + sFile);
                else
                    ImportHomeSurchargeWithHeader(@"c:\home\" + sFile);
                File.Copy(@"C:\Home\" + fi.Name, @"C:\Home]Processed\" + fi.Name);
                File.Delete(@"C:\Home\" + fi.Name);
            }
        }

        private void CopyVSC360()
        {
            var di = new DirectoryInfo(@"C:\inetpub\ftproot\VSC360\Home");
            FileInfo[] fiary = di.GetFiles("*.txt");
            string sFile;

            foreach (FileInfo fi in fiary)
            {
                sFile = fi.Name;
                FileSystem.FileCopy(@"C:\inetpub\ftproot\VSC360\Home\" + fi.Name, @"C:\Home\" + fi.Name);
                File.Delete(@"C:\inetpub\ftproot\VSC360\Home\" + fi.Name);
                if (!sFile.ToLower().Contains("surcharge"))
                    ImportHomeContractWithoutHeader(@"c:\home\" + sFile);
                else
                    ImportHomeSurchargeWithoutHeader(@"c:\home\" + sFile);

                FileSystem.FileClose(1);
                FileSystem.FileCopy(@"C:\Home\" + fi.Name, @"C:\Home\Processed\" + fi.Name);
                File.Delete(@"C:\Home\" + fi.Name);
            }
        }


        private void RateNewContracts()
        {
            SQL = "select * from contract where credate >= '" + DateTime.Today + "' ";
            //    'SQL = SQL + "where saledate = '10 / 1 / 2021' "
            //'SQL = SQL + "where not contractid in (select contractid from ContractAmt where RateTypeID = 1) "
            //'SQL = SQL + "where contractno like 'HAS10510219' "

            clC.OpenDB(SQL, sCON);
            if (clC.RowCount() > 0)
            {
                for (int i = 0; i < clC.RowCount(); i++)
                {
                    clC.GetRowNo(i);
                    ClearRate();
                    ProcessContractRate();
                    ProcessContractSurchargeRate();
                }
            }
        }

        private void ProcessContractSurchargeRate()
        {
            long lRateSheet = GetRateSheet(Convert.ToInt64(clC.GetFields("ratebookid")));
            SQL = "select * from contractsurcharge where contractid = " + clC.GetFields("contractid");
            clS.OpenDB(SQL, sCON);
            if (clS.RowCount() > 0)
            {
                for (int i = 0; i < clS.RowCount();i++)
                {
                    clS.GetRowNo(i);
                    ContractSurchargeRating(lRateSheet);
                }
            }
        }

        private void ClearRate()
        {
            clsDBO clcA = new clsDBO();
            SQL = "delete contractamt where contractid = " + clC.GetFields("contractid");
            clcA.RunSQL(SQL, sCON);
        }

        private void ProcessContractRate()
        {
            long lRateSheet = GetRateSheet(Convert.ToInt64(clC.GetFields("ratebookid")));
            ContractRating(lRateSheet);
        }

        private void ContractSurchargeRating(long xRateSheet)
        {
            clsDBO clR = new clsDBO();
            clsDBO clCA = new clsDBO();
            SQL = "select * from ratesurcharges ";
            SQL = SQL + "where ratesheetid = " + xRateSheet + " ";
            SQL = SQL + "and surchargeid = " + clS.GetFields("surchargeid") + " ";
            SQL = SQL + "and plantypeid = " + clC.GetFields("plantypeid") + " ";
            SQL = SQL + "and deductid = " + clC.GetFields("deductid") + " ";
            SQL = SQL + "and termmonth = " + clC.GetFields("termmonth") + " ";
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                for (int i = 0; i < clR.RowCount(); i++)
                {
                    clR.GetRowNo(i);
                    SQL = "select * from contractamt ";
                    SQL = SQL + "where contractid = " + clC.GetFields("contractid") + " ";
                    SQL = SQL + "and ratetypeid = " + clR.GetFields("ratetypeid") + " ";
                    clCA.OpenDB(SQL, sCON);
                    if (clCA.RowCount() > 0)
                    {
                        clCA.GetRow();
                        clCA.SetFields("amt", (Convert.ToDouble(clCA.GetFields("amt")) + Convert.ToDouble(clR.GetFields("surchargeamt"))).ToString());
                        clCA.OpenDB(SQL, sCON);
                    }
                    else
                    {
                        clCA.NewRow();
                        clCA.SetFields("contractid", clC.GetFields("contractid"));
                        clCA.SetFields("ratetypeid", clR.GetFields("ratetypeid"));
                        clCA.SetFields("amt", clR.GetFields("surchargeamt"));
                    }

                    if (clCA.RowCount() == 0)
                        clCA.AddRow();

                    clCA.SaveDB();
                }
            }
        }

        private void ContractRating(long xRateSheet)
        {
            clsDBO clR = new clsDBO();
            clsDBO clCA = new clsDBO();
            SQL = "select * from ratesheetdetail ";
            SQL = SQL + "where ratesheetid = " + xRateSheet + " ";
            SQL = SQL + "and plantypeid = " + clC.GetFields("plantypeid") + " ";
            SQL = SQL + "and deductid = " + clC.GetFields("deductid") + " ";
            SQL = SQL + "and termmonth = " + clC.GetFields("termmonth") + " ";
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                for (int i = 0; i < clR.RowCount(); i++)
                {
                    clR.GetRowNo(i);
                    SQL = "select * from contractamt ";
                    SQL = SQL + "where contractid = " + clC.GetFields("contractid") + " ";
                    SQL = SQL + "and ratetypeid = " + clR.GetFields("ratetypeid") + " ";
                    clCA.OpenDB(SQL, sCON);
                    if (clCA.RowCount() > 0)
                        clCA.GetRow();
                    else
                    {
                        clCA.NewRow();
                        clCA.SetFields("contractid", clC.GetFields("contractid"));
                        clCA.SetFields("ratetypeid", clR.GetFields("ratetypeid")); 
                    }

                    clCA.SetFields("amt", clR.GetFields("amt"));
                    if (clCA.RowCount() == 0)
                        clCA.AddRow();

                    clCA.SaveDB();
                }
            }
        }

        private long GetRateSheet(long xRateBookID)
        {
            clsDBO clR = new clsDBO();
            SQL = "select * from ratebook where ratebookid = " + xRateBookID;
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return Convert.ToInt64(clR.GetFields("ratesheetid"));
            }

            return 0;
        }

        private void ImportHomeSurchargeWithHeader(string xFileName)
        {
            long lContractID, lSurchargeID;
            clsDBO clCS = new clsDBO();
            FileSystem.FileOpen(1, xFileName, OpenMode.Input);
            sLine = FileSystem.LineInput(1);

            while (!FileSystem.EOF(1))
            {
                sLine = FileSystem.LineInput(1);
                sIn = sLine.Split('\t');
                lContractID = GetContractID(sIn[0]);
                if (sIn[2] == "CONDO LESS 5,000 SQ FT" && lProgramID == 12)
                {
                    SQL = "update contract ";
                    SQL = SQL + "set plantypeid = 13 ";
                    SQL = SQL + "where contractid = " + lContractID;
                    clCS.RunSQL(SQL, sCON);
                    goto MoveNext;
                }
                if (sIn[2] == "HOME 5,000 TO 8,000 SQ FT" && lProgramID == 12)
                {
                    SQL = "update contract ";
                    SQL = SQL + "set plantypeid = 14 ";
                    SQL = SQL + "where contractid = " + lContractID;
                    clCS.RunSQL(SQL, sCON);
                    goto MoveNext;
                }
                if (sIn[2] == "CONDO LESS 5,000 SQ FT" && lProgramID != 12)
                    SQL = SQL;

                if (sIn[2] == "HOME 5,000 TO 8,000 SQ FT" && lProgramID != 12)
                    SQL = SQL;

                lSurchargeID = GetSurchargeID(sIn[1]);
                SQL = "select * from contractsurcharge ";
                SQL = SQL + "where contractid = " + lContractID + " ";
                SQL = SQL + "and surchargeid = " + lSurchargeID + " ";
                clCS.OpenDB(SQL, sCON);
                if (clCS.RowCount() == 0)
                {
                    clCS.NewRow();
                    clCS.SetFields("contractid", lContractID.ToString());
                    clCS.SetFields("surchargeid", lSurchargeID.ToString());
                    clCS.SetFields("surchargeamt", "179.11");
                    clCS.AddRow();
                    clCS.SaveDB();
                }
            MoveNext:;

            }

            FileSystem.FileClose(1);
        }

        private void ImportHomeSurchargeWithoutHeader(string xFileName)
        {
            long lContractID, lSurchargeID;
            clsDBO clCS = new clsDBO();
            FileSystem.FileOpen(1, xFileName, OpenMode.Input);
            //sLine = FileSystem.LineInput(1);

            while (!FileSystem.EOF(1))
            {
                sLine = FileSystem.LineInput(1);
                sIn = sLine.Split('\t');
                lContractID = GetContractID(sIn[0]);
                if (sIn[2] == "CONDO LESS 5,000 SQ FT" && lProgramID == 12)
                {
                    SQL = "update contract ";
                    SQL = SQL + "set plantypeid = 13 ";
                    SQL = SQL + "where contractid = " + lContractID;
                    clCS.RunSQL(SQL, sCON);
                    goto MoveNext;
                }
             
                if (sIn[2] == "CONDO LESS 5,000 SQ FT" && lProgramID != 12)
                    SQL = SQL;

                if (sIn[2] == "HOME 5,000 TO 8,000 SQ FT" && lProgramID != 12)
                    SQL = SQL;

                lSurchargeID = GetSurchargeID(sIn[1]);
                SQL = "select * from contractsurcharge ";
                SQL = SQL + "where contractid = " + lContractID + " ";
                SQL = SQL + "and surchargeid = " + lSurchargeID + " ";
                clCS.OpenDB(SQL, sCON);
                if (clCS.RowCount() == 0)
                {
                    clCS.NewRow();
                    clCS.SetFields("contractid", lContractID.ToString());
                    clCS.SetFields("surchargeid", lSurchargeID.ToString());
                    clCS.SetFields("surchargeamt", "179.11");
                    clCS.AddRow();
                    clCS.SaveDB();
                }
            MoveNext:;

            }

            FileSystem.FileClose(1);
        }

        private long GetSurchargeID(string xSurcharge)
        {
            clsDBO clS = new clsDBO();
            string sTemp = xSurcharge.Trim();
            if (sTemp == "Fridge2nd")
                sTemp = "SecondRef";

            SQL = "select * from surcharge where surchargecode = '" + xSurcharge + "' ";
            clS.OpenDB(SQL, sCON);
            if (clS.RowCount() > 0)
            {
                clS.GetRow();
                return Convert.ToInt64(clS.GetFields("surchargeid"));
            }

            return 0;
        }

        private long GetContractID(string xContractNo)
        {
            clsDBO clC = new clsDBO();
            SQL = "select * from contract where contractno = '" + xContractNo + "' ";
            clC.OpenDB(SQL, sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                lProgramID = Convert.ToInt64(clC.GetFields("contractid"));
                return Convert.ToInt64(clC.GetFields("plantypeid"));
            }

            return 0;
        }

        private void ImportHomeContractWithHeader(string xFileName)
        {
            FileSystem.FileOpen(1, xFileName, OpenMode.Input);
            sLine = FileSystem.LineInput(1);
            while (!FileSystem.EOF(1))
            {
                sLine = FileSystem.LineInput(1);
                sIn = sLine.Split('\t');
                ProcessContract();
            }
            FileSystem.FileClose(1);
        }

        private void ImportHomeContractWithoutHeader(string xFileName)
        {
            FileSystem.FileOpen(1, xFileName, OpenMode.Input);
            while (!FileSystem.EOF(1))
            {
                sLine = FileSystem.LineInput(1);
                sIn = sLine.Split('\t');
                ProcessContractRate();
            }
        }

        private void ProcessContract()
        {
            string sContractNo = sIn[25];
            if (!VerifyContractNo(sContractNo))
                return;

            clsDBO clC = new clsDBO();
            SQL = "select * from contract where contractno = '" + sContractNo + "' ";
            clC.OpenDB(SQL, sCON);
            if (clC.RowCount() == 0)
            {
                clC.NewRow();
                clC.SetFields("contractno", sContractNo);
                clC.SetFields("fname", sIn[0]);
                clC.SetFields("lname", sIn[1]);
                clC.SetFields("addr1", sIn[2]);
                clC.SetFields("addr2", sIn[3]);
                clC.SetFields("city", sIn[4]);
                clC.SetFields("state", sIn[5]);
                clC.SetFields("zip", sIn[6]);
                sIn[7] = sIn[7].Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");
                clC.SetFields("phone", sIn[7]);
                clC.SetFields("status", "Pending");
                clC.SetFields("coveredaddr1", sIn[8]);
                clC.SetFields("coveredaddr2", sIn[9]);
                clC.SetFields("coveredcity", sIn[10]);
                clC.SetFields("coveredstate", sIn[11]);
                clC.SetFields("coveredzip", sIn[12]);
                clC.SetFields("dealerid", GetDealerID(sIn[13]).ToString());
                clC.SetFields("moxydealercost", sIn[15]);
                clC.SetFields("customercost", sIn[16]);
                clC.SetFields("retailrate", sIn[17]);
                clC.SetFields("markup", sIn[18]);
                clC.SetFields("surchargecost", sIn[19]);
                clC.SetFields("saledate", sIn[20]);

                DateTime d1 = Convert.ToDateTime(sIn[20]);
                DateTime d2 = Convert.ToDateTime("9/13/2021");
                if (DateTime.Compare(d1, d2) < 0)
                {
                    if (sIn[21].ToLower() == "appliance")
                        clC.SetFields("plantypeid", "1");
                    if (sIn[21].ToLower() == "system")
                        clC.SetFields("plantypeid", "2");
                    if (sIn[21].ToLower() == "total")
                        clC.SetFields("plantypeid", "3");
                }
                else
                {
                    if (sIn[21].ToLower() == "appliance")
                        clC.SetFields("plantypeid", "4");
                    if (sIn[21].ToLower() == "system")
                        clC.SetFields("plantypeid", "8");
                    if (sIn[21].ToLower() == "total")
                        clC.SetFields("plantypeid", "12");
                }

                if (sIn[22] == "75")
                    clC.SetFields("deductid", "1");

                clC.SetFields("termmonth", sIn[23]);
                clC.SetFields("effdate", Convert.ToDateTime(clC.GetFields("effdate")).AddDays(Convert.ToInt32(sIn[24])).ToString());
                clC.SetFields("expdate", Convert.ToDateTime(clC.GetFields("effdate")).AddMonths(Convert.ToInt32(clC.GetFields("termmonth"))).ToString());
                if (sIn[26].ToLower() == "hpp")
                    clC.SetFields("programid", "1");

                clC.SetFields("ratebookid", CalcRateBook(Convert.ToInt64(clC.GetFields("programid"))).ToString());
                clC.SetFields("lienholder", sIn[28]);
                var client = new WebClient();
                if (sIn[30].Length > 0)
                {
                    client.DownloadFile(sIn[30], @"C:\inetpub\wwwroot\VeritasGenerator\wwwroot\Home\wwwroot\Documents\Contracts\" + clC.GetFields("contractno") + " - dec.pdf");
                    clC.SetFields("decpage", "https://veritasgenerator.com/home/documents/contracts/" + clC.GetFields("contractno") + "-dec.pdf");
                }
                if (sIn[31].Length > 0)
                {
                    client.DownloadFile(sIn[31], @"C:\inetpub\wwwroot\VeritasGenerator\wwwroot\Home\wwwroot\Documents\Contracts\" + clC.GetFields("contractno") + " - TC.pdf");
                    clC.SetFields("tcpage", "https://veritasgenerator.com/home/documents/contracts/" + clC.GetFields("contractno") + "-TC.pdf");
                }

                clC.SetFields("creby", "1");
                clC.SetFields("credate", DateTime.Today.ToString());
                clC.AddRow();
                clC.SaveDB();
            }
        }

        private long CalcRateBook(long xProgramID)
        {
            clsDBO clRB = new clsDBO();
            SQL = "select * from ratebook ";
            SQL = SQL + "where programid = " + xProgramID + " ";
            SQL = SQL + "and startdate <= '" + DateTime.Today + "' ";
            SQL = SQL + "and enddate >= '" + DateTime.Today + "' ";
            clRB.OpenDB(SQL, sCON);
            if (clRB.RowCount() > 0)
            {
                clRB.GetRow();
                return Convert.ToInt64(clRB.GetFields("ratebookid"));
            }

            return 0;
        }

        private long GetDealerID(string sDealerNo)
        {
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer where dealerno = '" + sDealerNo + "' ";
            clD.OpenDB( SQL, sCON);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                return Convert.ToInt64(clD.GetFields("dealerid"));
            }

            return 0;
        }

        private bool VerifyContractNo(string xContractNo)
        {
            clsDBO clc = new clsDBO();
            SQL = "select * from contract where contractno = '" + xContractNo + "' ";
            clc.OpenDB(SQL, sCON);
            if (clc.RowCount() > 0)
                return false;

            return true;
        }
    }


}
