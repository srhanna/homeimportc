﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace HomeImportC
{
    internal class clsDBO
    {
        private string sSQL;
        private DataSet ds;
        private DataRow dr;
        private string sConnectionString;

        public void AddRow()
        {
            ds.Tables[0].Rows.Add(dr);
        }
        public void GetRowNo(int xNo)
        {
            dr = ds.Tables[0].Rows[xNo];
        }
        public void GetRow()
        {
            dr = ds.Tables["DBO"].Rows[0];
        }
        public void NewRow()
        {
            dr = ds.Tables[0].NewRow();
        }
        public string GetFields(string xFieldName)
        {
            if ((dr[xFieldName]) != null)
            {
                return dr[xFieldName].ToString();
            }
            else
            {
                return "";
            }
        }
        public void SetFields(string xFieldName, string value)
        {
            if (value.Length > 0)
            {
                value = value.Replace("'", "");
            }
            dr[xFieldName] = value;
        }
        public int RowCount()
        {
            try
            {
                return ds.Tables[0].Rows.Count;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public DataSet GetData(string xSQL, string xConnectionString)
        {
            try
            {
                ds = new DataSet();
                sSQL = xSQL;
                SqlConnection conn = new SqlConnection(xConnectionString);
                SqlDataAdapter da = new SqlDataAdapter(sSQL, conn);
                da.MissingSchemaAction = MissingSchemaAction.Add;
                da.Fill(ds, "DBO");
                da.Dispose();
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                string sTemp = e.Message;
                throw;
            }
            return ds;
        }
        public void OpenDB(string xSQL, string xConnectionString)
        {
        MoveHere:
            try
            {
                ds = new DataSet();
                sSQL = xSQL;
                sConnectionString = xConnectionString;
                SqlConnection conn = new SqlConnection(sConnectionString);
                SqlDataAdapter da = new SqlDataAdapter(sSQL, conn);
                da.SelectCommand.CommandTimeout = 120;
                da.MissingSchemaAction = MissingSchemaAction.Add;
                int test = da.Fill(ds, "DBO");
                da.Dispose();
                conn.Close();
                conn.Dispose();
            }
            catch (Exception e)
            {
                string sTemp = e.Message;
                if (sTemp.Contains("restore"))
                {
                    System.Threading.Thread.Sleep(60000);
                    goto MoveHere;
                }
            }
        }
        public void RunSQL(string xSQL, string xConnectionString)
        {
            try
            {
                SqlConnection conn = new SqlConnection(xConnectionString);
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = xSQL;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void SaveDB()
        {
            DataSet dsChange = ds.GetChanges();
            if (!ds.HasChanges())
            {
                return;
            }
            SqlConnection conn = new SqlConnection(sConnectionString);
            SqlDataAdapter da = new SqlDataAdapter(sSQL, conn);
            SqlCommandBuilder cb = new SqlCommandBuilder(da);
            da.Update(dsChange, "DBO");
            dsChange.Dispose();
            ds.Dispose();
            cb.Dispose();
            conn.Close();
            conn.Dispose();
            conn = null;
            cb = null;
            ds = null;
            dr = null;
            da = null;
            dsChange = null;
        }
    }
}
