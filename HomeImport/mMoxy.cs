﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeImportC
{
    internal static class mMoxy 
    {
        private static string SQL;
        private static string sCON = "server=104.168.205.130;database=veritashome;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E";
        private static string sCONM = "server=198.143.98.122;database=veritasmoxy;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E";
        private static clsDBO clMC= new clsDBO();
        private static clsDBO clMCS = new clsDBO();
        private static long lContractID, lSurchargeID;

        public static void ImportMoxy()
        {
            SQL = "select * from moxyhome where dealstatus = 'Sold' ";
            clMC.OpenDB(SQL, sCONM);
            if (clMC.RowCount() > 0)
            {
                for (int i = 0; i < clMC.RowCount(); i++)
                {
                    clMC.GetRowNo(i);
                    if (CheckContract())
                        goto MoveNext;

                    ImportContract();
                    ImportSurcharge();

                MoveNext:;
                }
            }
        }

        private static void ImportSurcharge()
        {
            clsDBO clR = new clsDBO();
            SQL = "select * from moxyhomesurcharge where dealid = " + clMC.GetFields("dealid") + " ";
            clMCS.OpenDB(SQL, sCONM);
            if (clMCS.RowCount() > 0)
            {
                for (int i = 0; i < clMCS.RowCount();i++)
                {
                    clMCS.GetRowNo(i);
                    GetContractID();
                    if (lContractID == 0)
                        goto MoveHere;
                    GetSurchargeID();
                    SQL = "select * from contractsurcharge where contractid = " + lContractID + " " +
                        "and surchargeid = " + lSurchargeID + " ";
                    clR.OpenDB(SQL, sCON);
                    if (clR.RowCount() == 0)
                    {
                        clR.NewRow();
                        clR.SetFields("contractid", lContractID.ToString());
                        clR.SetFields("surchargeid", lSurchargeID.ToString());
                        clR.SetFields("surchargeamt", clMCS.GetFields("surchargeamt"));
                        clR.AddRow();
                        clR.SaveDB();
                    }
                MoveHere:;
                }
            }
        }

        private static void GetSurchargeID()
        {
            clsDBO clR = new clsDBO();
            lSurchargeID = 0;
            SQL = "select * from surcharge where surchargecode = '" + clMCS.GetFields("surchargecode") + "' ";
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lSurchargeID = Convert.ToInt64(clR.GetFields("surchargeid"));
            }
        }

        private static void GetContractID()
        {
            clsDBO clR = new clsDBO();
            lContractID = 0;
            SQL = "select * from contract where contractno = '" + clMC.GetFields("contractno") + "' ";
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                lContractID = Convert.ToInt64(clR.GetFields("contractid"));
            }
        }

        private static long GetDealerID(string sDealerNo)
        {
            clsDBO clD = new clsDBO();
            SQL = "select * from dealer where dealername = '" + sDealerNo + "' ";
            clD.OpenDB(SQL, sCON);
            if (clD.RowCount() > 0)
            {
                clD.GetRow();
                return Convert.ToInt64(clD.GetFields("dealerid"));
            }

            return 0;

            //If GetDealerID = 0 Then
            //    GetDealerID = GetDealerID
            //End If

        }

        private static void ImportContract()
        {
            clsDBO clC = new clsDBO();
            SQL = "select * from contract where contractno = '" + clMC.GetFields("contractno") + "' ";
            clC.OpenDB(SQL, sCON);
            if (clC.RowCount() == 0)
            {
                clC.NewRow();
                clC.SetFields("contractno", clMC.GetFields("contractno"));
                clC.SetFields("fname", clMC.GetFields("customerfname"));
                clC.SetFields("lname", clMC.GetFields("customerlname"));
                clC.SetFields("addr1", clMC.GetFields("customeraddr1"));
                clC.SetFields("addr2", clMC.GetFields("customeraddr2"));
                clC.SetFields("city", clMC.GetFields("customercity"));
                clC.SetFields("state", clMC.GetFields("customerstate"));
                clC.SetFields("zip", clMC.GetFields("customerzip"));
                clC.SetFields("phone", clMC.GetFields("customerphone"));
                clC.SetFields("status", "Pending");
                clC.SetFields("coveredaddr1", clMC.GetFields("customeraddr1"));
                clC.SetFields("coveredaddr2", clMC.GetFields("customeraddr2"));
                clC.SetFields("coveredcity", clMC.GetFields("customercity"));
                clC.SetFields("coveredstate", clMC.GetFields("customerstate"));
                clC.SetFields("coveredzip", clMC.GetFields("customerzip"));
                clC.SetFields("dealerid", GetDealerID(clMC.GetFields("dealername")).ToString());
                clC.SetFields("moxydealercost", clMC.GetFields("dealercost"));
                clC.SetFields("customercost", clMC.GetFields("customercost"));
                clC.SetFields("retailrate", clMC.GetFields("retailcost"));
                clC.SetFields("markup", clMC.GetFields("markup"));
                clC.SetFields("surchargecost", clMC.GetFields("totalsurcharge"));
                clC.SetFields("saledate", clMC.GetFields("solddate"));
                clC.SetFields("plantypeid", "0");

                DateTime d1 = Convert.ToDateTime(clMC.GetFields("solddate"));
                DateTime d2 = Convert.ToDateTime("9/13/2021");
                if (DateTime.Compare(d1, d2) < 0)
                {
                    if (clMC.GetFields("vscplan").ToLower().Contains("appliance"))
                        clC.SetFields("plantypeid", "1");
                    if (clMC.GetFields("vscplan").ToLower().Contains("system"))
                        clC.SetFields("plantypeid", "2");
                    if (clMC.GetFields("vscplan").ToLower().Contains("total"))
                        clC.SetFields("plantypeid", "3");
                }
                else
                {
                    if (clMC.GetFields("vscplan").ToLower().Contains("appliance"))
                    {
                        clC.SetFields("plantypeid", "4");
                        if (clMC.GetFields("vscplan").ToLower().Contains("condo"))
                            clC.SetFields("plantypeid", "5");
                    }

                    if (clMC.GetFields("vscplan").ToLower().Contains("system")) {
                        clC.SetFields("plantypeid", "8");
                        if (clMC.GetFields("vscplan").ToLower().Contains("condo"))
                            clC.SetFields("plantypeid", "9");
                        
                    }
                    if (clMC.GetFields("vscplan").ToLower().Contains("total")) {
                        clC.SetFields("plantypeid", "12");
                        if (clMC.GetFields("vscplan").ToLower().Contains("condo"))
                            clC.SetFields("plantypeid", "13");
                    }
                }

                if (clC.GetFields("plantypeid") == "0")
                    clC.SetFields("plantypeid", clC.GetFields("plantypeid"));

                if (clMC.GetFields("Deductible") == "75")
                    clC.SetFields("deductid", "1");

                if (clMC.GetFields("Deductible") == "0")
                    clC.SetFields("deductid", "2");

                clC.SetFields("termmonth", clMC.GetFields("termmonth"));
                clC.SetFields("effdate", Convert.ToDateTime(clC.GetFields("saledate")).AddDays(30).ToString());
                clC.SetFields("expdate", Convert.ToDateTime(clC.GetFields("effdate")).AddMonths(Convert.ToInt32(clC.GetFields("termmonth"))).ToString());
                clC.SetFields("programid", "1");
                clC.SetFields("ratebookid", "1");
                clC.SetFields("lienholder", "");
                clC.SetFields("creby", "1");
                clC.SetFields("credate", DateTime.Today.ToString());
                clC.AddRow();
                clC.SaveDB();
            }
        }

        private static bool CheckContract()
        {
            clsDBO clR = new clsDBO();
            SQL = "select * from contract where contractno = '" + clMC.GetFields("contractno") + "' ";
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
                return true;

            return false;
        }
       
    }
}
