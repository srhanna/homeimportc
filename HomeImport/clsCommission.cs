﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeImportC
{
    internal class clsCommission
    {
        private long lcontractID;
        private string SQL, sCancelDate;
        private string sCON = "server=104.168.205.130;database=veritashome;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E";
        private clsDBO clC = new clsDBO();
        private clsDBO clComm = new clsDBO();
        private double dPer;


        public long ContractID
        {
            get { return lcontractID; }
            set { lcontractID = value; }
        }

        public void CalcComm()
        {
            clsDBO clR = new clsDBO();
            SQL = "select * from contract where contractid = " + lcontractID;
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                for (int i = 0; i < clR.RowCount(); i++)
                {
                    clR.GetRowNo(i);
                    ContractID = Convert.ToInt64(clR.GetFields("contractid"));
                    ProcessContract();
                }
            }
        }

        public void ProcessContract()
        {
            ClearCommission();
            SQL = "select * from contract where contractid = " + lcontractID;
            clC.OpenDB(SQL, sCON);
            if (clC.RowCount() > 0)
            {
                clC.GetRow();
                GetCommission();
            }
        }

        private void GetCommission() 
        {
            SQL = "select * from commissions where dealerid = " + clC.GetFields("dealerid") + " ";
            SQL = SQL + "where startdate <= '" + clC.GetFields("saledate") + "' and enddate >= '" + clC.GetFields("saledate") + "' ";
            clComm.OpenDB(SQL, sCON);
            if (clComm.RowCount() > 0)
            {
                for (int i = 0; i < clComm.RowCount();i++)
                {
                    clComm.GetRowNo(i);
                    CalcCancel();
                    FillContractAmt();
                    FillContractCommission();
                }
            }
        }

        private void FillContractCommission()
        {
            clsDBO clR = new clsDBO();
            SQL = "insert into contractcommissions " +
                "(contractid, ratetypeid, payeeid, amt, datepaid, cancelamt, canceldate, commissionper) values (" +
                lcontractID + ", " + clComm.GetFields("ratetypeid") + ", " + clComm.GetFields("payeeid") + ", " +
                clComm.GetFields("amt") + ", ";
            if (clC.GetFields("datepaid").Length > 0)
                SQL = SQL + "'" + clC.GetFields("datepaid") + "', ";
            else
                SQL = SQL + "null, ";
            SQL = SQL + Convert.ToDouble(clComm.GetFields("amt")) * dPer + ", ";
            if (sCancelDate.Length > 0)
                SQL = SQL + "'" + sCancelDate + "', ";
            else
                SQL = SQL + "null, ";
            SQL = SQL + GetCommissionPer() + ")";
            clR.RunSQL(SQL, sCON);
        }

        private double GetCommissionPer()
        {
            clsDBO clR = new clsDBO();
            SQL = "select * from payee where payeeid = " + clComm.GetFields("payeeid");
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                return Convert.ToDouble(clR.GetFields("commissionper"));
            }

            return 1;
        }

        private void FillContractAmt()
        {
            clsDBO clR = new clsDBO();
            SQL = "insert contractamt (contractid, ratetypeid, amt, cancelamt) values (" +
                lcontractID + ", " + clComm.GetFields("ratetypeid") + ", " + clComm.GetFields("amt") + ", " +
                (Convert.ToDouble(clComm.GetFields("amt")) * dPer) + ") ";
            clR.RunSQL(SQL, sCON);
        }

        private void CalcCancel()
        {
            clsDBO clR = new clsDBO();
            dPer = 0;
            sCancelDate = "";
            SQL = "select * from contractcancel where contractid = " + lcontractID + " and cancelstatus = 'Cancelled' ";
            clR.OpenDB(SQL, sCON);
            if (clR.RowCount() > 0)
            {
                clR.GetRow();
                dPer = Convert.ToDouble(clR.GetFields("termfactor"));
                sCancelDate = clR.GetFields("canceldate");
            }
        }

        private void ClearCommission()
        {
            clsDBO clR = new clsDBO();
            SQL = "delete contractamt where contractid = " + lcontractID + " and ratetypeid in (select ratetypeid from ratetype where ratecategoryid = 3) ";
            clR.RunSQL(SQL, sCON);
            SQL = "delete contractcommissions where contractid = " + lcontractID;
            clR.RunSQL(SQL, sCON);
        }
    }
}
